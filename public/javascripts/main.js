
var currentNav="#login_button_nav";

$("#profileImage").click(function(e) {
    $("#imageUpload").click();
});

function fasterPreview( uploader ) {
    if ( uploader.files && uploader.files[0] ){
        $('#profileImage').attr('src',
            window.URL.createObjectURL(uploader.files[0]) );
    }
}

$("#imageUpload").change(function(){
    fasterPreview( this );
});
function displayAddUser() {
    $('#container').load('add_modal',(position) =>{});

    // window.location.hash = 'newUser';
}
function displayUpdateUser() {
    console.log('displayUpdateUser----');
    $('#container').load('update_modal',(position) =>{});

}
function displayRemoveUser() {
    $('#container').load('removeUser',(position) =>{});

}
function onUsersLoad(posi) {
    if(posi === 'client') {
        $('#newUserButton').hide();
    }
    /*if(posi ==='worker')
        displayUsersWorker();*/
}
function onLoadAddUser(posi) {
    if(posi === 'worker') {
        $('#workerOption').hide();
        $('#managerOption').hide();
    }
}
function onLoadUpdateUser(posi) {
    console.log('++++');
    if(posi === 'worker') {
        $('#workerOption').hide();
        $('#managerOption').hide();
    }
    // $('#submitbtn').click(false);
    document.getElementById('updateBtn').style.display='none';
}
//function of button refresh
function refresh(posi) {
    debugger;
    if (posi === 'manager') {
        $('#li_flowers').show();
        $('#li_branches').show();
        $('#li_users').show();
        $('#li_home').show();
        $('#li_contact').show();
        $('#loginButton').hide();
        $('#li_users_worker').hide();
        $('#logoutButton').show();
    }
    if (posi === 'worker') {
        $('#li_flowers').show();
        $('#li_branches').hide();
        $('#li_users').hide();
        $('#li_users_worker').show();
        $('#li_home').show();
        $('#li_contact').show();
        $('#loginButton').hide();
        $('#logoutButton').show();
    }
    if (posi === 'client') {
        $('#li_flowers').show();
        $('#li_branches').hide();
        $('#li_users').hide();
        $('#li_users_worker').hide();
        $('#li_home').show();
        $('#li_contact').show();
        $('#loginButton').hide();
        $('#logoutButton').show();
    }
    if (posi === ''||posi === undefined) {
        $('#li_flowers').hide();
        $('#li_branches').hide();
        $('#li_users').hide();
        $('#li_users_worker').hide();
        $('#li_home').show();
        $('#li_contact').hide();
        $('#loginButton').show();
        $('#logoutButton').hide();
    }
}

function displayFlowers() {
    $('#container').load('flowers',() =>{
        $(currentNav).removeClass("active");
        currentNav="#li_flowers";
        $(currentNav).addClass("active");
    });
    closeNav();
    window.location.hash = 'flowers';
}

function displayContact() {
    $('#container').load('contact',() =>{
        $(currentNav).removeClass("active");
        currentNav="#li_conact";
        $(currentNav).addClass("active");
    });
    closeNav();
    window.location.hash = 'contact';
}

function displayBranches() {
    $('#container').load('branches',() =>{
        $(currentNav).removeClass("active");
        currentNav="#li_branches";
        $(currentNav).addClass("active");
    });
    closeNav();
    window.location.hash = 'branches';
}

function displayAbout(position) {
    $('#container').load('about',() =>{
        $(currentNav).removeClass("active");
        currentNav="#li_home";
        $(currentNav).addClass("active");
    });
    closeNav();
    window.location.hash = 'about';
}

function displayUsers() {
    $('#container').load('users', () => {
        $(currentNav).removeClass("active");
        currentNav = "#li_users";
        $(currentNav).addClass("active");
    });
    closeNav();
    window.location.hash = 'users';
}
function displayUsersWorker() {
    $('#container').load('users_worker', () => {
        $(currentNav).removeClass("active");
        currentNav = "#li_users_worker";
        $(currentNav).addClass("active");
    });
    closeNav();
    window.location.hash = 'users_worker';
}

function loadNavigation(position){
      if (position === 'manager') {
        $('#li_flowers').show();
        $('#li_branches').show();
        $('#li_users').show();
        $('#li_users_worker').hide();
        $('#li_home').show();
        $('#li_contact').show();

    }
    if (position === 'worker') {
        $('#li_flowers').show();
        $('#li_branches').hide();
        $('#li_users').hide();
        $('#li_users_worker').show();
        $('#li_home').show();
        $('#li_contact').show();
    }
    if (position === 'client') {
        $('#li_flowers').show();
        $('#li_branches').hide();
        $('#li_users_worker').hide();
        $('#li_users').hide();
        $('#li_home').show();
        $('#li_contact').show();
    }
    if (position === '') {
        $('#li_flowers').hide();
        $('#li_branches').hide();
        $('#li_users').hide();
        $('#li_users_worker').hide();
        $('#li_home').show();
        $('#li_contact').hide();
        $('#loginButton').show();
        $('#logoutButton').hide();
    }
}
function Logout()
{
    $('#li_flowers').hide();
    $('#li_branches').hide();
    $('#li_users').hide();
    $('#li_users_worker').hide();
    $('#li_home').show();
    $('#li_contact').hide();
    $('#loginButton').show();
    $('#logoutButton').hide();
    displayAbout();
}
async function searchUser()
{
    let search =$('#search').val()
    let response = await fetch("/users/all-users", {
        method: 'post',
    });
    let users = await response.json();
    users.data.forEach(function (user) {
        if(search == user.username){
            console.log(user.image);
            document.getElementById('userName').value =user.username;
            document.getElementById('password1').value ='';
            document.getElementById('userPosition').value =user.position;
            document.getElementById('fname').value =user.firstName;
            document.getElementById('lname').value =user.lastName;
            document.getElementById('Image').src =user.image;
            document.getElementById('updateBtn').style.display='block'
        }

    });
}
async function userByUsername(username)
{
    let search =username;
    let theUser= undefined;
    let response = await fetch("/users/all-users", {
        method: 'post',
    });
    let users = await response.json();
    users.data.forEach(function (user) {
        if(search == user.username){
            theUser= user;
        }
    });
    return theUser;
}

async function RemoveUser(){
    let user = new FormData();
    user.append("userName", $('#userName').val());
    let response = await fetch("/users/remove-user", {
        method: 'post',
        body: user
    });
}
async function updateUser(){
    let username=$('#userName').val();
    username= await userByUsername(username);
        let conPass = document.getElementById('confirmpass').value;
        let user = new FormData();
        user.append("userName", $('#userName').val());
        user.append("position", $('#userPosition').val());
        user.append("password", $('#password1').val());
        user.append('picture', username.image);
        user.append("fName", $('#fname').val());
        user.append("lName", $('#lname').val());
        user.append("status", true);
        if ($('#userName').val() == '' || $('#userPosition').val() == 'Chose option' || $('#password1').val() == '' || $('#fname').val() == '' || $('#lname').val() == '') {
            alert('fill all fields');
        } else {
            if (conPass == $('#password1').val()) {
                let response = await fetch("/users/update-user", {
                    method: 'post',
                    body: user
                });
            } else {
                console.log(conPass + '====' + $('#password1').val());
                alert('passwords are not identical');
            }
        }
}



check_user = async () => {
    let user_name = document.getElementById('name').value;
    let user_password = document.getElementById('password').value;
    const params = new URLSearchParams();
    params.append('userName', user_name);
    params.append('password', user_password);
    console.log('users_data.data.position');
    try {
        let response = await fetch("/userlogin", {
            method: 'post',
            body: params
        });
        let users_data = await response.json();
        console.log(users_data.data.position);
        if (response.status === 200) {
            saveToCookie(user_name , users_data.data.position)
            console.log("save name of user in cookie");

            document.getElementById('myModal').style.display = 'none';
            if (users_data.data.position === 'manager') {
                position ='manager';
                $('#li_flowers').show();
                $('#li_branches').show();
                $('#li_users').show();
                $('#li_users_worker').hide();
                $('#li_home').show();
                $('#li_contact').show();
                $('#loginButton').hide();
                $('#logoutButton').show();
            }
            if (users_data.data.position === 'worker') {
                position ='worker';
                $('#li_flowers').show();
                $('#li_branches').hide();
                $('#li_users').hide();
                $('#li_users_worker').show();
                $('#li_home').show();
                $('#li_contact').show();
                $('#loginButton').hide();
                $('#logoutButton').show();
            }
            if (users_data.data.position === 'client') {
                position ='client';
                $('#li_flowers').show();
                $('#li_branches').hide();
                $('#li_users').hide();
                $('#li_users_worker').hide();

                $('#li_home').show();
                $('#li_contact').show();
                $('#loginButton').hide();
                $('#logoutButton').show();
            }
            if (users_data.data.position === '') {
                position ='';
                $('#li_flowers').hide();
                $('#li_branches').hide();
                $('#li_users').hide();
                $('#li_users_worker').hide();
                $('#li_home').show();
                $('#li_contact').hide();
                $('#loginButton').show();
                $('#logoutButton').hide();
            }
        }

    } catch (error) { console.error('Error' + error); }
}

function updateFinish() {
    alert("Update Successfully");
    displayUsers();
}
