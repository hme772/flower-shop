
function hideCatalogButton() {
    $('#navflowers').hide();
}

function hideBranchButton() {
    $('#navbranches').hide();
}

function hideManagment() {
    $('#list-group').hide();
}

function hideWelcomePage() {
    $('#welcome_page').hide();
}

function hideAbout() {
    $('#tabAbout').hide();
}

function hideUserbutton() {
    $('#navusers').hide();
}

function hideCategories() {
    $('#categories').hide();
}

function showCatalogButton() {
    var catBtn = $('#navflowers');
    catBtn.show();
    $('#welcome_page').hide();


}

function changeToLogoutButton() {
    // language=JQuery-CSS
    let lg_btn = $('#login_button_nav');
    if(lg_btn) { // turn to logout
        lg_btn.hide();
        $('#logout_button_nav').show();
        $('#logout_button_nav').click(function () {
            logout();
            hideWelcomePage();
        });
    }

}

function showAbout() {
    var catBtn = $('#tabAbout');
    catBtn.show();
    $('#welcome_page').hide();/*
    $('#aboutPage').show();
    catBtn.click(function () {
        $.get("/about", function(data, status){

            $('#welcome_page').hide();
        });
    });*/
}

function showAllButtons(data) {
    showCatalogButton();
    showCategories();
    showAbout();

    if(data=="worker" || data=="manager")
    {
        if(data=="worker")
        {
            isWorker=true;
            isMangaer=false;
        }
        else
        {
            isWorker=false;
            isMangaer=true;
        }
        $('#navusers').show();
    }
    $('#navbranches').show();

}

function showCategories() {
    $('#categories').show();
}

$(function() {
    //hideManagment();
    //$('.col-md-3').hide();
    //hideCatalogButton();
    //hideCatalog();
    $('#logout_button_nav').hide();
    hideCategories();
    hideCatalogButton();
    hideAbout();
    hideBranchButton();
    hideUserbutton();
    //$('#categories').hide();
});
