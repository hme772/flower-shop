const debug = require("debug")("mongo:model-user");
const mongo = require("mongoose");

module.exports = db => {

    let schema = new mongo.Schema({
        username: { type: String, required: true, unique: true, index: true },
        position: String,
        password: { type: String, required: true },
        image: String,
        firstName: String,
        lastName: String,
        status: Boolean,
    }, { autoIndex: false });

    schema.statics.CREATE = async function(user) {
        try{
            return this.create({
                username: user[0],
                position: user[1],
                password: user[2],
                image: user[3],
                firstName: user[4],
                lastName: user[5],
                status: user[6]
            });
        } catch (e) {
            alert(user[0])
        }

    };

    schema.statics.findByUserByUsernamePassword = function(username, password) {
        return this.find({username: username, password: password});
    };

    schema.statics.REQUEST = async function() {
        return this.find({});
    };

    schema.statics.UPDATE = async function(username, user) {
        try{
        return this.updateOne(
            {"username" : username},
            {$set: { 'username': user[0],
                    'position': user[1],
                    'password': user[2],
                    'image': user[3],
                    'firstName': user[4],
                    'lastName': user[5],
                    'status': user[6]}});
                } catch (e) {
                    alert(user[0])
                }
    };
    schema.statics.DELETE = async function(user) {
        try{
        return this.updateOne(
            {"username" : user[0]},
            {$set: { 'username': user[0],
                    'position': user[1],
                    'password': user[2],
                    'image': user[3],
                    'firstName': user[4],
                    'lastName': user[5],
                    'status': user[6]}});
                } catch (e) {
                    alert(user[0])
                }
    };
    // the schema is useless so far
    // we need to create a model using it
    // db.model('User', schema, 'User'); // (model, schema, collection)
    db.model('User', schema); // if model name === collection name
    debug("User model created");
};
