const debug = require("debug")("mongo:model-branch");
const mongo = require("mongoose");

module.exports = db => {

    let schema = new mongo.Schema({
        key: { type: Number, required: true, unique: true, index: true },
        name: String,
        address: String,
        image: String,
        phone: String,
        status: Boolean,
    }, { autoIndex: false });

    schema.statics.CREATE = async function(branch) {
        return this.create({
            key: branch[0],
            name: branch[1],
            address: branch[2],
            image: branch[3],
            phone: branch[4],
            status: branch[5],
        });
    };

    schema.methods.findByKey = function(key) {
        return this.find({key});
    };

    schema.statics.REQUEST = async function() {
        return this.find({});
    };

    // the schema is useless so far
    // we need to create a model using it
    // db.model('User', schema, 'User'); // (model, schema, collection)
    db.model('Branch', schema); // if model name === collection name
    debug("Branch model created");
};
