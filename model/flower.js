const debug = require("debug")("mongo:model-flower");
const mongo = require("mongoose");

module.exports = db => {

    let schema = new mongo.Schema({
        name: { type: String, required: true, unique: true, index: true },
        color: String,
        price: Number,
        image: String,
    }, { autoIndex: false });

    schema.statics.CREATE = async function(flower) {
        try{
            return this.create({
            name: flower[0],
            color: flower[1],
            price: flower[2],
            image: flower[3]
        });
    }
        catch (e) {
            alert(flower[0])
        }

    };
    schema.statics.findByName = function(name) {
        return this.find({ name: name });
      };

    schema.methods.findFlowerByName = function(name) {
        return this.find({name});
    }


    schema.statics.REQUEST = async function() {
        return this.find({});
    };

    // the schema is useless so far
    // we need to create a model using it
    // db.model('User', schema, 'User'); // (model, schema, collection)
    db.model('Flower', schema); // if model name === collection name
    debug("Flower model created");
};
