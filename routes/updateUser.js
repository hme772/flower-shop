var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    res.render('update_modal');
});

module.exports = router;
