var express = require('express');
var router = express.Router();
// let branches = require('../models/branches');
const Branch = require('../model')("Branch");


router.get('/', function (req, res) {
    console.log("Branch");
    Branch.REQUEST().then((branches) => {
        res.render('branches', {branches: branches});
    });
});

module.exports = router;