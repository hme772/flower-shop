var express = require('express');
var router = express.Router();
const User = require('../model')("User");
const path = require('path');
var multer = require('multer');
const uuid = require('uuid');

var upload=multer({dest: 'C:/Users/hoday/labs/t4/tar4/public/images'});
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'C:/Users/hoday/labs/t4/tar4/public/images')
    },
    filename: function (req, file, cb) {
        cb(null, uuid() + path.extname(file.originalname));
    }
});

var upload = multer({ storage: storage });

router.get('/', function (req, res) {
    User.REQUEST().then((users) => {
        let actives =[];
        users.forEach(user => {
            if(user.status){
                actives.push(user);
            }
        });
        res.render('users', {users: actives});
    });
});

router.get('/addNewUser',function (req, res) {
    User.REQUEST().then((users) => {
        res.render('users', {users: users});
    });
});

router.post('/all-users',async (req,res)=>{
    User.REQUEST().then((users) => {
        let actives =[];
        users.forEach(user => {
            if(user.status){
                actives.push(user);
            }
        });
    res.status(200).json({
        message: 'ok',
        data: actives,
    })
    })
});
// router.get('/newUser', function (req, res) {
//     console.log('==============');
//     res.render('newUser');
// });
router.post('/updateUser',async function (req, res, next) {
    console.log('99');
    let user = [];
    user[0] = req.body.username;
    user[1] = req.body.position;
    user[2] = req.body.password;
    user[3] =  req.body.image;
    user[4] = req.body.firstName;
    user[5] = req.body.lastName;
    user[6] =req.body.status;
    console.log(user);
    let response=User.UPDATE(user[0], user);
    res.status(200).json({
        message: 'ok'
    })
});
router.post('/deleteUser',async function (req, res, next) {
    console.log("99   ++++++++++++++++++++++++++");
    let user = [];
    user[0] = req.body.username;
    user[1] = req.body.position;
    user[2] = req.body.password;
    user[3] =  req.body.image;
    user[4] = req.body.firstName;
    user[5] = req.body.lastName;
    user[6] =false;
    console.log(req.body);
    let response=User.DELETE(user);
    res.status(200).json({
        message: 'ok'
    })
});

router.post('/add'/*,  upload.single('image')*/,async function (req, res, next) {
    console.log('/add');
    console.log(req.body);
    let user = [];
    user[0] = req.body.userName;
    user[1] = req.body.position;
    user[2] = req.body.password;
    user[3] =  req.body.image.replace("C:\\fakepath\\", "images/");
    // user[3] =  "images/" ;
    user[4] = req.body.firstName;
    user[5] = req.body.lastName;
    user[6] =req.body.status;
    console.log(user);
    let response=User.CREATE(user);
    console.log(response);
    res.status(200).json({
            message: 'ok'
        })   

});
module.exports = router;
