var express = require('express');
var router = express.Router();
const User = require('../model')("User");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express',
    webStatus: "out",
    position: '----',
    aboutPage: 'yes',
    userStatus: 'none'  });
});

module.exports = router;
