var express = require('express');
var router = express.Router();
const User = require('../model')("User");
const path = require('path');
var multer = require('multer');
const uuid = require('uuid');

var upload=multer({dest: 'C:/Users/hoday/labs/t4/tar4/public/images'});
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'C:/Users/hoday/labs/t4/tar4/public/images')
    },
    filename: function (req, file, cb) {
        cb(null, uuid() + path.extname(file.originalname));
    }
});

var upload = multer({ storage: storage });

router.get('/', function (req, res) {
    User.REQUEST().then((users) => {
        let actives =[];
        users.forEach(user => {
            if(user.status){
                actives.push(user);
            }
        });
        res.render('users_worker', {users: actives});
    });
});

router.get('/addNewUser',function (req, res) {
    User.REQUEST().then((users) => {
        res.render('users_worker', {users: users});
    });
});

router.post('/all-users',async (req,res)=>{
    User.REQUEST().then((users) => {
        let actives =[];
        users.forEach(user => {
            if(user.status && user.position==='client'){
                actives.push(user);
            }
        });
    res.status(200).json({
        message: 'ok',
        data: actives,
    })
    })
});
// router.get('/newUser', function (req, res) {
//     console.log('==============');
//     res.render('newUser');
// });
router.post('/update-user',  upload.single('picture'),async function (req, res, next) {
    console.log('99');
    let user = [];
    user[0] = req.body.userName;
    user[1] = req.body.position;
    user[2] = req.body.password;
    user[3] =  req.body.picture;
    user[4] = req.body.lName;
    user[5] = req.body.fName;
    user[6] =req.body.status;
    console.log(user);
    User.UPDATE(user[0], user);
});
router.post('/remove-user',  upload.single('picture'),async function (req, res, next) {
    User.DELETE(req.body.userName);
});

router.post('/add',  upload.single('picture'),async function (req, res, next) {
    console.log('/add');
    let user = [];
    user[0] = req.body.userName;
    user[1] = req.body.position;
    user[2] = req.body.password;
    user[3] =  "images/"+req.file.filename ;
    // user[3] =  "images/" ;
    user[4] = req.body.lName;
    user[5] = req.body.fName;
    user[6] =req.body.status;
    console.log(user);
    User.CREATE(user);

});
module.exports = router;
