var express = require('express');
var router = express.Router();
var multer = require('multer');
const Flower = require('../model')("Flower");
const uuid = require('uuid');
const path = require('path');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'view/public/images')
  },
  filename: function (req, file, cb) {

    cb(null, uuid() + path.extname(file.originalname));
  }
});

var upload = multer({ storage: storage });


router.get('/', function (req, res) {
    Flower.REQUEST().then((flowers) => {
        res.render('flowers', {flowers: flowers});
    });
});
router.post('/all-flowers',async (req,res)=>{
  Flower.REQUEST().then((flowers) => {
      let f =[];
      flowers.forEach(flower => {
              f.push(flower);
      });
  res.status(200).json({
      message: 'ok',
      data: f,
  })
  })
});



router.post('/add'/*, upload.single("picture")*/, async function(req, res) {
    //const flower = JSON.parse(req.body.flower);
    console.log(req.body);
    let flowerToAdd = [];
    flowerToAdd[0] = req.body.fname;
    flowerToAdd[1] = req.body.color;
    flowerToAdd[2] = req.body.price;
    flowerToAdd[3] = req.body.image.replace("C:\\fakepath\\", "images/");
    console.log(flowerToAdd);

    let response= await Flower.CREATE(flowerToAdd);
    res.status(200).json({
      message: 'ok'
  })
});

router.get('/find', function(req, res) {
    Flower.findByName(req.body.flowerName).then((ans) => {
    });
});

module.exports = router;
